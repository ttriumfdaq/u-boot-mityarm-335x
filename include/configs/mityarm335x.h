/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef __CONFIG_MITYARM335X_H
#define __CONFIG_MITYARM335X_H

#define CONFIG_MITYARM335X
#include <configs/ti_am335x_common.h>

#define MACH_TYPE_MITYARM335X		3903
#define CONFIG_MACH_TYPE		MACH_TYPE_MITYARM335X

/* Clock related defines */
#define V_OSCK				24000000 /* Clock output from T2 */
#define V_SCLK				(V_OSCK)

#define CONFIG_DISPLAY_CPUINFO
#define CONFIG_CMD_STRINGS

/* Always 128 KiB env size */
#define CONFIG_ENV_SIZE  (128 << 10)

#ifdef CONFIG_MITYARM_4K_PAGE_NAND
#ifdef CONFIG_MITYARM_1GB_NAND
#define CONFIG_NAND_SRC_ADDR	"0x480000"
#else
#define CONFIG_NAND_SRC_ADDR	"0x340000"
#endif /* CONFIG_MITYARM_1GB_NAND */
#else
#define CONFIG_NAND_SRC_ADDR	"0x280000"
#endif

/* Disable SPL direct boot */
/* When booting from nand, this will skip 2nd level u-boot. Would take some
 * setup to get to work.  Disabled to keep old usage. */
#undef CONFIG_SPL_OS_BOOT

/*
 * memtest works on 64 MB in DRAM after skipping 64 from
 * start addr of ram disk
 */
#define CONFIG_CMD_MEMTEST
#define CONFIG_SYS_MEMTEST_START       (CONFIG_SYS_SDRAM_BASE \
					+ (64 * 1024 * 1024))
#define CONFIG_SYS_MEMTEST_END         (CONFIG_SYS_MEMTEST_START \
                                        + (64 * 1024 * 1024))

#ifdef CONFIG_MITYARM_4K_PAGE_NAND
#define ENV_NAND_ROOT "ubi0:rootfs rw ubi.mtd=8,4096"
#else
#define ENV_NAND_ROOT "ubi0:rootfs rw ubi.mtd=8,2048"
#endif

#define CONFIG_EXTRA_ENV_SETTINGS \
	"bootfile=uImage\0" \
	"loadaddr=0x82000000\0" \
	"kloadaddr=0x80007fc0\0" \
	"script_addr=0x81900000\0" \
	"console=ttyO0,115200n8\0" \
	"mmc_dev=0\0" \
	"mmc_root=/dev/mmcblk0p2 rw\0" \
	"nand_root=" ENV_NAND_ROOT "\0" \
	"mmc_root_fs_type=ext3 rootwait\0" \
	"nand_root_fs_type=ubifs rootwait=1\0" \
        "nand_src_addr=" CONFIG_NAND_SRC_ADDR "\0" \
	"nand_img_siz=0x500000\0" \
	"rootpath=/export/rootfs\0" \
	"nfsopts=nolock\0" \
	"static_ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}" \
			"::off\0" \
	"ip_method=none\0" \
        "bootenv=uenv.txt\0" \
	"loadbootenv=fatload mmc ${mmc_dev} ${loadaddr} ${bootenv}\0" \
	"importbootenv=echo Importing environment from mmc ...; " \
		"env import -t $loadaddr $filesize\0" \
	"mmc_load_uimage=fatload mmc ${mmc_dev} ${kloadaddr} ${bootfile}\0" \
	"optargs=\0" \
	"bootargs_defaults=setenv bootargs " \
		"console=${console} " \
		"${optargs}\0" \
	"mmc_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=${mmc_root} " \
		"rootfstype=${mmc_root_fs_type} ip=${ip_method}\0" \
	"nand_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=${nand_root} noinitrd " \
		"rootfstype=${nand_root_fs_type} ip=${ip_method}\0" \
	"net_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=/dev/nfs " \
		"nfsroot=${serverip}:${rootpath},${nfsopts} rw " \
		"ip=dhcp\0" \
	"mmc_boot=run mmc_args; " \
		"run mmc_load_uimage; " \
		"bootm ${kloadaddr}\0" \
	"nand_boot=echo Booting from nand ...; " \
		"run nand_args; " \
		"nandecc hw 2; " \
		"nand read.i ${kloadaddr} ${nand_src_addr} ${nand_img_siz}; " \
		"bootm ${kloadaddr}\0" \
	"net_boot=echo Booting from network ...; " \
		"setenv autoload no; " \
		"dhcp; " \
		"tftp ${kloadaddr} ${bootfile}; " \
		"run net_args; " \
		"bootm ${kloadaddr}\0" \

#define CONFIG_BOOTCOMMAND \
	"if mmc rescan; then " \
		"echo SD/MMC found on device ${mmc_dev};" \
		"if run loadbootenv; then " \
			"echo Loaded environment from ${bootenv};" \
			"run importbootenv;" \
		"fi;" \
		"if test -n $uenvcmd; then " \
			"echo Running uenvcmd ...;" \
			"run uenvcmd;" \
		"fi;" \
		"if run mmc_load_uimage; then " \
			"run mmc_args;" \
			"bootm ${kloadaddr};" \
		"fi;" \
	"fi;" \
	"run nand_boot;" \

/* Calls misc_init_r() in som.c */
#define CONFIG_MISC_INIT_R
/* Include commands icache/dcache */
#define CONFIG_CMD_CACHE

/* SPI support */
#ifdef CONFIG_SPI
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_CMD_SF
#define CONFIG_SF_DEFAULT_SPEED		48000000
#define CONFIG_SF_DEFAULT_BUS		1
#define CONFIG_OMAP3_SPI_D0_D1_SWAPPED
#endif

/* PMIC support */
#define CONFIG_POWER_TPS65910

/* SPL */
#define CONFIG_SPL_YMODEM_SUPPORT
#define CONFIG_SPL_POWER_SUPPORT
#define CONFIG_SPL_LDSCRIPT		"$(CPUDIR)/am33xx/u-boot-spl.lds"

#ifdef CONFIG_NAND
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION

/* Large page nand */
#ifdef CONFIG_MITYARM_4K_PAGE_NAND
#define CONFIG_SYS_NAND_PAGE_SIZE       4096
#define CONFIG_SYS_NAND_OOBSIZE         224
#ifdef CONFIG_MITYARM_1GB_NAND
#define CONFIG_SYS_NAND_BLOCK_SIZE      (128*CONFIG_SYS_NAND_PAGE_SIZE) /* 512K */
#else
#define CONFIG_SYS_NAND_BLOCK_SIZE      (64*CONFIG_SYS_NAND_PAGE_SIZE) /* 256K */
#endif
#define CONFIG_SYS_NAND_BAD_BLOCK_POS	NAND_LARGE_BADBLOCK_POS

#define CONFIG_SYS_NAND_ECCPOS {\
				2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,\
				16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,\
				28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,\
				40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,\
				52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,\
				64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,\
				76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,\
				88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,\
				100, 101, 102, 103, 104, 105, 106, 107, 108, 109,\
				110, 111, 112, 113, 114, 115, 116, 117, 118, 119,\
				120, 121, 122, 123, 124, 125, 126, 127, 128, 129,\
				130, 131, 132, 133, 134, 135, 136, 137, 138, 139,\
				140, 141, 142, 143, 144, 145, 146, 147, 148, 149,\
				150, 151, 152, 153, 154, 155, 156, 157, 158, 159,\
				160, 161, 162, 163, 164, 165, 166, 167, 168, 169,\
				170, 171, 172, 173, 174, 175, 176, 177, 178, 179,\
				180, 181, 182, 183, 184, 185, 186, 187, 188, 189,\
				190, 191, 192, 193, 194, 195, 196, 197, 198, 199,\
				200, 201, 202, 203, 204, 205, 206, 207, 208, 209}

#define CONFIG_SYS_NAND_ECCSIZE		512
#define CONFIG_SYS_NAND_ECCBYTES	26

#define CONFIG_SYS_NAND_ECCSTEPS	8
#define	CONFIG_SYS_NAND_ECCTOTAL	(CONFIG_SYS_NAND_ECCBYTES * \
						CONFIG_SYS_NAND_ECCSTEPS)
#else /* CONFIG_MITYARM_4K_PAGE_NAND */
#define CONFIG_SYS_NAND_PAGE_SIZE       2048
#define CONFIG_SYS_NAND_OOBSIZE         64
#define CONFIG_SYS_NAND_BLOCK_SIZE      (64*CONFIG_SYS_NAND_PAGE_SIZE)
#define CONFIG_SYS_NAND_BAD_BLOCK_POS	NAND_LARGE_BADBLOCK_POS
#define CONFIG_SYS_NAND_ECCPOS		{ 2, 3, 4, 5, 6, 7, 8, 9, \
					 10, 11, 12, 13, 14, 15, 16, 17, \
					 18, 19, 20, 21, 22, 23, 24, 25, \
					 26, 27, 28, 29, 30, 31, 32, 33, \
					 34, 35, 36, 37, 38, 39, 40, 41, \
					 42, 43, 44, 45, 46, 47, 48, 49, \
					 50, 51, 52, 53, 54, 55, 56, 57, }

#define CONFIG_SYS_NAND_ECCSIZE		512
#define CONFIG_SYS_NAND_ECCBYTES	14

#define CONFIG_SYS_NAND_ECCSTEPS	4
#define	CONFIG_SYS_NAND_ECCTOTAL	(CONFIG_SYS_NAND_ECCBYTES * \
						CONFIG_SYS_NAND_ECCSTEPS)
#endif /* CONFIG_MITYARM_4K_PAGE_NAND */

#define CONFIG_SYS_NAND_PAGE_COUNT	(CONFIG_SYS_NAND_BLOCK_SIZE / \
					 CONFIG_SYS_NAND_PAGE_SIZE)

#undef CONFIG_SYS_NAND_U_BOOT_OFFS /* Clear default value */
#define CONFIG_SYS_NAND_U_BOOT_OFFS	(4 * CONFIG_SYS_NAND_BLOCK_SIZE) /* just after 4 x MLO copies*/
#define	CONFIG_SYS_NAND_U_BOOT_START	CONFIG_SYS_TEXT_BASE

#endif /* CONFIG_NAND */

/* NS16550 Configuration */
#define CONFIG_SYS_NS16550_COM1		0x44e09000	/* Baseboard has UART0 */
#define CONFIG_BAUDRATE			115200

/*
 * select serial console configuration
 */
#define CONFIG_SERIAL1			1
#define CONFIG_CONS_INDEX		1

/*
 * USB configuration
 */
#define CONFIG_USB_MUSB_DSPS
#define CONFIG_ARCH_MISC_INIT
#define CONFIG_MUSB_GADGET
#define CONFIG_MUSB_PIO_ONLY
#define CONFIG_MUSB_DISABLE_BULK_COMBINE_SPLIT
#define CONFIG_USB_GADGET
#define CONFIG_USBDOWNLOAD_GADGET
#define CONFIG_USB_GADGET_DUALSPEED
#define CONFIG_USB_GADGET_VBUS_DRAW	2
#define CONFIG_MUSB_HOST
#define CONFIG_AM335X_USB0
#define CONFIG_AM335X_USB0_MODE	MUSB_PERIPHERAL
#define CONFIG_AM335X_USB1
#define CONFIG_AM335X_USB1_MODE MUSB_HOST

#ifdef CONFIG_MUSB_HOST
#define CONFIG_CMD_USB
#define CONFIG_USB_STORAGE
#endif /* CONFIG_MUSB_HOST */

#ifdef CONFIG_MUSB_GADGET
#define CONFIG_USB_ETHER
#define CONFIG_USB_ETH_RNDIS
#define CONFIG_USBNET_HOST_ADDR	"de:ad:be:af:00:00"

/* USB TI's IDs */
#define CONFIG_G_DNL_VENDOR_NUM 0x0403
#define CONFIG_G_DNL_PRODUCT_NUM 0xBD00
#define CONFIG_G_DNL_MANUFACTURER "Texas Instruments"
#endif /* CONFIG_MUSB_GADGET */

#if defined(CONFIG_SPL_BUILD) && defined(CONFIG_SPL_USBETH_SUPPORT)
/* disable host part of MUSB in SPL */
#undef CONFIG_MUSB_HOST
#endif

/* Unsupported features */
#undef CONFIG_USE_IRQ
#if defined(CONFIG_NO_ETH)
# undef CONFIG_CMD_NET
# undef CONFIG_DRIVER_TI_CPSW
# undef CONFIG_MII
# undef CONFIG_BOOTP_DEFAULT
# undef CONFIG_BOOTP_DNS
# undef CONFIG_BOOTP_DNS2
# undef CONFIG_BOOTP_SEND_HOSTNAME
# undef CONFIG_BOOTP_GATEWAY
# undef CONFIG_BOOTP_SUBNETMASK
# undef CONFIG_NET_RETRY_COUNT		10
#else
#ifdef CONFIG_AM335X_TF
# define CONFIG_ETH_PORT 1  /* Port 1 on the switch */
#else
# define CONFIG_ETH_PORT 2  /* Port 2 on the switch */
#endif
#endif

/* Network */
#if defined(CONFIG_CMD_NET)
# define CONFIG_PHY_GIGE
# define CONFIG_CMD_MII
# define CONFIG_PHYLIB
# define CONFIG_PHY_ADDR			1 //Unused due to CPSW_SEARCH_PHY
# define CONFIG_CPSW_SEARCH_PHY
# define CONFIG_CPSW_SEARCH_PHY_MASK	(1 << 1 | 1 << 2) // Seach for phy ids 1 and 2
# define CONFIG_PHY_SMSC
# define CONFIG_PHY_VITESSE
# define CONFIG_PHY_MICREL
# define CONFIG_SYS_VSC8601_SKEWFIX
# define CONFIG_SYS_VSC8601_SKEW_TX      0
# define CONFIG_SYS_VSC8601_SKEW_RX      3
#endif

/* NAND support */
#ifdef CONFIG_NAND
#define GPMC_NAND_ECC_LP_x8_LAYOUT	1

/* ENV in NAND */
#if defined(CONFIG_NAND)
#undef CONFIG_ENV_IS_NOWHERE
#define CONFIG_ENV_IS_IN_NAND
#define CONFIG_SYS_MAX_FLASH_SECT	2048            /* max no of sectors in a chip */
#ifdef CONFIG_MITYARM_4K_PAGE_NAND
#ifdef CONFIG_MITYARM_1GB_NAND
# define MNAND_ENV_OFFSET		(0x400000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(512 << 10)	/* 512 KiB */
#else
# define MNAND_ENV_OFFSET		(0x300000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(256 << 10)	/* 256 KiB */
#endif
#else
# define MNAND_ENV_OFFSET		(0x260000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(128 << 10)	/* 128 KiB */
#endif
#define CONFIG_ENV_OFFSET		MNAND_ENV_OFFSET
#define CONFIG_ENV_ADDR			MNAND_ENV_OFFSET

/* Not using redundent flash ... just reserving the next block to be written if the first block is bad
#define CONFIG_ENV_OFFSET_REDUND        (0xA0000)
#define CONFIG_ENV_SIZE_REDUND          CONFIG_ENV_SIZE
*/
#endif  /* ENV in NAND */

/* defines for UBI FS in NAND */
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_RBTREE
#define CONFIG_LZO
/* un-comment for UBIFS debug
#define CONFIG_UBIFS_FS_DEBUG
#define CONFIG_UBIFS_FS_DEBUG_MSG_LVL 3
#define CONFIG_UBIFS_FS_DEBUG_CHECKS
#define UBIFS_MSG_FLAGS_DEFAULT 0xFFFFFFFF
*/
#define MTDIDS_DEFAULT "nand0=nand"
#ifdef CONFIG_MITYARM_4K_PAGE_NAND
#ifdef CONFIG_MITYARM_1GB_NAND
# define MTDPARTS_DEFAULT "mtdparts=nand:2048k@0(mlo),2048k@2048k(uboot),512k@4096k(env),5m@4608k(kernel),-@9728k(userfs)"
#else
# define MTDPARTS_DEFAULT "mtdparts=nand:1024k@0(mlo),2048k@1024k(uboot),256k@3072k(env),5m@3328k(kernel),515840k(userfs)"
#endif
#else
# define MTDPARTS_DEFAULT "mtdparts=nand:512k@0(mlo),1920k@512k(uboot),128k@2432k(env),5m@2560k(kernel),97152k(userfs)"
#endif

#endif /* NAND support */

/*
 * NOR Size = 16 MB
 * No.Of Sectors/Blocks = 128
 * Sector Size = 128 KB
 * Word lenght = 16 bits
 */
#if defined(CONFIG_NOR)
# undef CONFIG_ENV_IS_NOWHERE
# undef CONFIG_SYS_MALLOC_LEN
# define CONFIG_SYS_FLASH_USE_BUFFER_WRITE 1
# define CONFIG_SYS_MALLOC_LEN		(0x100000)
# define CONFIG_SYS_FLASH_CFI
# define CONFIG_FLASH_CFI_DRIVER
# define CONFIG_FLASH_CFI_MTD
# define CONFIG_SYS_MAX_FLASH_SECT	128
# define CONFIG_SYS_MAX_FLASH_BANKS	1
# define CONFIG_SYS_FLASH_BASE		(0x08000000)
# define CONFIG_SYS_MONITOR_BASE	CONFIG_SYS_FLASH_BASE
# define NOR_SECT_SIZE			(128 * 1024)
#if defined (CONFIG_NOR_ENV)
# define CONFIG_ENV_IS_IN_FLASH	1
# define CONFIG_SYS_ENV_SECT_SIZE	(NOR_SECT_SIZE)
# define CONFIG_ENV_SECT_SIZE		(NOR_SECT_SIZE)
# define CONFIG_ENV_OFFSET		(0 * NOR_SECT_SIZE) /* base of NOR */
# define CONFIG_ENV_ADDR		(CONFIG_SYS_FLASH_BASE + \CONFIG_ENV_OFFSET)
#endif /* CONFIG_NOR_ENV */
# define CONFIG_MTD_DEVICE
#endif	/* NOR support */

#endif	/* ! __CONFIG_MITYARM335X_H */
