/*
 * som.c
 *
 * Copyright (C) 2012 Critical Link LLC - http://www.criticallink.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#include <common.h>
#include <errno.h>
#include <spl.h>
#include <asm/arch/cpu.h>
#include <asm/arch/hardware.h>
#include <asm/arch/omap.h>
#include <asm/arch/ddr_defs.h>
#include <asm/arch/clock.h>
#include <asm/arch/gpio.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mem.h>
#include <asm/io.h>
#include <asm/emif.h>
#include <asm/gpio.h>
#include <i2c.h>
#include <miiphy.h>
#include <cpsw.h>
#include <nand.h>
#include <power/tps65910.h>
#include "common_def.h"
#include "config_block.h"

/* from drivers/mtd/nand/omap_gpmc.c */
extern void omap_nand_print_ecc_info(void);

DECLARE_GLOBAL_DATA_PTR;

static struct ctrl_dev *cdev = (struct ctrl_dev *)CTRL_DEVICE_BASE;

/* Profile 0 is the dev kit board, profile 1 is the test fixture */
#ifdef CONFIG_AM335X_TF
static __maybe_unused unsigned char profile = PROFILE_1;
#else
static __maybe_unused unsigned char profile = PROFILE_0;
#endif

/* #define TRACE_FUNC*/
#ifdef TRACE_FUNC
#ifdef CONFIG_SPL_BUILD
#define tfenter do { \
	printf ("SPL %s:%d %s\n", __FILE__, __LINE__, __func__); } while (0)
#else
#define tfenter do { \
	printf ("%s:%d %s\n", __FILE__, __LINE__, __func__); } while (0)
#endif
#else
#define tfenter
#endif
/* UART Defines */
#define UART_SYSCFG_OFFSET	(0x54)
#define UART_SYSSTS_OFFSET	(0x58)

#define UART_RESET		(0x1 << 1)
#define UART_CLK_RUNNING_MASK	0x1
#define UART_SMART_IDLE_EN	(0x1 << 0x3)

/* Timer Defines */
#define TSICR_REG		0x54
#define TIOCP_CFG_REG		0x10
#define TCLR_REG		0x38

/* DDR defines */
#define MDDR_SEL_DDR2           0xefffffff /* IOs set for DDR2-STL Mode */
#define CKE_NORMAL_OP           0x00000001 /* Normal Op:CKE controlled by EMIF */
#define GATELVL_INIT_MODE_SEL   0x1	/* Selects a starting ratio value based
					   on DATA0/1_REG_PHY_GATELVL_INIT_RATIO_0
					   value programmed by the user */
#define WRLVL_INIT_MODE_SEL     0x1	/* Selects a starting ratio value based
					   on DATA0/1_REG_PHY_WRLVL_INIT_RATIO_0
					   value programmed by the user */

/*
 * I2C Address of various board
 */
#define I2C_EEPROM_ADDR		0x50
#define I2C_EEPROM_BUS		1
#define I2C_TPS65910_CTL_BUS	2
#define I2C_TPS65910_SMT_BUS	1

/* 
 * Phy Reset GPIO 3[10]
 */
#define GPIO_PHY_RESET_N	106

/* TLK110 PHY registers */
#define TLK110_COARSEGAIN_REG	0x00A3
#define TLK110_LPFHPF_REG	0x00AC
#define TLK110_SPAREANALOG_REG	0x00B9
#define TLK110_VRCR_REG		0x00D0
#define TLK110_SETFFE_REG	(unsigned char)0x0107
#define TLK110_FTSP_REG		(unsigned char)0x0154
#define TLK110_ALFATPIDL_REG	0x002A
#define TLK110_PSCOEF21_REG	0x0096
#define TLK110_PSCOEF3_REG	0x0097
#define TLK110_ALFAFACTOR1_REG	0x002C
#define TLK110_ALFAFACTOR2_REG	0x0023
#define TLK110_CFGPS_REG	0x0095
#define TLK110_FTSPTXGAIN_REG	(unsigned char)0x0150
#define TLK110_SWSCR3_REG	0x000B
#define TLK110_SCFALLBACK_REG	0x0040
#define TLK110_PHYRCR_REG	0x001F

/* TLK110 register writes values */
#define TLK110_COARSEGAIN_VAL	0x0000
#define TLK110_LPFHPF_VAL	0x8000
#define TLK110_SPAREANALOG_VAL	0x0000
#define TLK110_VRCR_VAL		0x0008
#define TLK110_SETFFE_VAL	0x0605
#define TLK110_FTSP_VAL		0x0255
#define TLK110_ALFATPIDL_VAL	0x7998
#define TLK110_PSCOEF21_VAL	0x3A20
#define TLK110_PSCOEF3_VAL	0x003F
#define TLK110_ALFAFACTOR1_VAL	0xFF80
#define TLK110_ALFAFACTOR2_VAL	0x021C
#define TLK110_CFGPS_VAL	0x0000
#define TLK110_FTSPTXGAIN_VAL	0x6A88
#define TLK110_SWSCR3_VAL	0x0000
#define TLK110_SCFALLBACK_VAL	0xC11D
#define TLK110_PHYRCR_VAL	0x4000
#define TLK110_PHYIDR1		0x2000
#define TLK110_PHYIDR2		0xA201

#define NO_OF_MAC_ADDR          3
#define ETH_ALEN		6

#ifdef CONFIG_SPL_BUILD
static const int nand_page_size = CONFIG_SYS_NAND_PAGE_SIZE;
static ulong ram_size = 0;

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot(void)
{
	/* break into full u-boot on 'c' */
	return (serial_tstc() && serial_getc() == 'c');
}
#endif

/*
 * Perform a memory test. A more complete alternative test can be
 * configured using CONFIG_SYS_ALT_MEMTEST. The complete test loops until
 * interrupted by ctrl-c or by a failure of one of the sub-tests.
 */
static int mtest(ulong *apStart, ulong *apEnd)
{
	vu_long *addr, *start, *end;
	ulong val;
	ulong readback;
	ulong errs = 0;
	ulong err_limit = 8;	/* Really 1, but show up to 32 */
	int iterations = 1;
	int iteration_limit = 2;

	ulong incr;
	ulong pattern = 0;

	if (apStart)
		start = apStart;
	else
		start = (ulong *) CONFIG_SYS_MEMTEST_START;

	if (apEnd)
		end = apEnd;
	else
		end = (ulong *) (CONFIG_SYS_MEMTEST_END);

	incr = 1;
	printf("Testing RAM from %08x to %08x\n", (unsigned int)start,
	       (unsigned int)end);
	for (;;) {
		if (iteration_limit && (iterations > iteration_limit)) {
			printf("Tested %d iteration(s) with %lu errors.\n",
			       iterations - 1, errs);
			return errs != 0;
		}
		++iterations;

		printf("\rPattern %08lX  Writing..."
		       "%12s" "\b\b\b\b\b\b\b\b\b\b", pattern, "");

		for (addr = start, val = pattern; addr < end; addr++)
			*addr = val;

		printf("Done\nValidating...");

		errs = 0;
		for (addr = start, val = pattern;
		     (errs < err_limit) && (addr < end); addr++) {
			readback = *addr;
			if (readback != val) {
				printf("Mem error @ 0x%08X: "
				       "found %08lX, expected %08lX\n",
				       (uint) (uintptr_t) addr, readback, val);
				errs++;
			}
		}
		if (errs > err_limit) {
			printf("Error limit[%d] exceeded!\n", (int)err_limit);
			return 1;
		}

		/*
		 * Flip the pattern each time to make lots of zeros and
		 * then, the next time, lots of ones.  We decrement
		 * the "negative" patterns and increment the "positive"
		 * patterns to preserve this feature.
		 */
		if (pattern & 0x80000000)
			pattern = -pattern;	/* complement & increment */
		else
			pattern = ~pattern;
		incr = -incr;
	}
	return 0;
}
#endif /* CONFIG_SPL_BUILD */

/* set the D3 LED to on of off. This LED is hooked up to the GPIO0
 * output of the PMIC.
 * \param[in] b if non-zero, turn LED on, else off
 */
void set_led_d3(int b)
{
	unsigned char v;
	/* save the bus number */
	int bus = i2c_get_bus_num();
	if(bus != I2C_TPS65910_CTL_BUS)
		i2c_set_bus_num(I2C_TPS65910_CTL_BUS);

	i2c_read(TPS65910_CTRL_I2C_ADDR, TPS65910_GPIO0_REG, 1, &v, 1);
	/* Ensure GPIO pin set as output */
	v |= TPS65910_GPIO0_REG_GPIO_CFG;
	/* Ensure GPIO pull-up is disabled, pull-up exists on board */
	v &= ~TPS65910_GPIO0_REG_GPIO_PUEN;

	if (b)
		v |= TPS65910_GPIO0_REG_GPIO_SET;
	else
		v &= ~TPS65910_GPIO0_REG_GPIO_SET;

	i2c_write(TPS65910_CTRL_I2C_ADDR, TPS65910_GPIO0_REG, 1, &v, 1);

	/* put it back */
	if(bus != I2C_TPS65910_CTL_BUS)
		i2c_set_bus_num(bus);
}

#ifdef CONFIG_SPL_BUILD

static const struct ddr_data ddr2_data = {
	.datardsratio0 = ((MT47H128M16RT25E_RD_DQS << 30) |
			  (MT47H128M16RT25E_RD_DQS << 20) |
			  (MT47H128M16RT25E_RD_DQS << 10) |
			  (MT47H128M16RT25E_RD_DQS << 0)),
	.datawdsratio0 = ((MT47H128M16RT25E_WR_DQS << 30) |
			  (MT47H128M16RT25E_WR_DQS << 20) |
			  (MT47H128M16RT25E_WR_DQS << 10) |
			  (MT47H128M16RT25E_WR_DQS << 0)),
	.datawiratio0 = ((MT47H128M16RT25E_PHY_WRLVL << 30) |
			 (MT47H128M16RT25E_PHY_WRLVL << 20) |
			 (MT47H128M16RT25E_PHY_WRLVL << 10) |
			 (MT47H128M16RT25E_PHY_WRLVL << 0)),
	.datagiratio0 = ((MT47H128M16RT25E_PHY_GATELVL << 30) |
			 (MT47H128M16RT25E_PHY_GATELVL << 20) |
			 (MT47H128M16RT25E_PHY_GATELVL << 10) |
			 (MT47H128M16RT25E_PHY_GATELVL << 0)),
	.datafwsratio0 = ((MT47H128M16RT25E_PHY_FIFO_WE << 30) |
			  (MT47H128M16RT25E_PHY_FIFO_WE << 20) |
			  (MT47H128M16RT25E_PHY_FIFO_WE << 10) |
			  (MT47H128M16RT25E_PHY_FIFO_WE << 0)),
	.datawrsratio0 = ((MT47H128M16RT25E_PHY_WR_DATA << 30) |
			  (MT47H128M16RT25E_PHY_WR_DATA << 20) |
			  (MT47H128M16RT25E_PHY_WR_DATA << 10) |
			  (MT47H128M16RT25E_PHY_WR_DATA << 0)),
	.datauserank0delay = MT47H128M16RT25E_PHY_RANK0_DELAY,
	.datadldiff0 = PHY_DLL_LOCK_DIFF,
};

static const struct cmd_control ddr2_cmd_ctrl_data = {
	.cmd0csratio = MT47H128M16RT25E_RATIO,
	.cmd0dldiff = MT47H128M16RT25E_DLL_LOCK_DIFF,
	.cmd0iclkout = MT47H128M16RT25E_INVERT_CLKOUT,

	.cmd1csratio = MT47H128M16RT25E_RATIO,
	.cmd1dldiff = MT47H128M16RT25E_DLL_LOCK_DIFF,
	.cmd1iclkout = MT47H128M16RT25E_INVERT_CLKOUT,

	.cmd2csratio = MT47H128M16RT25E_RATIO,
	.cmd2dldiff = MT47H128M16RT25E_DLL_LOCK_DIFF,
	.cmd2iclkout = MT47H128M16RT25E_INVERT_CLKOUT,
};

static const struct emif_regs ddr2_emif_reg_data = {
	.sdram_config = MT47H128M16RT25E_EMIF_SDCFG,
	.ref_ctrl = MT47H128M16RT25E_EMIF_SDREF,
	.sdram_tim1 = MT47H128M16RT25E_EMIF_TIM1,
	.sdram_tim2 = MT47H128M16RT25E_EMIF_TIM2,
	.sdram_tim3 = MT47H128M16RT25E_EMIF_TIM3,
	.emif_ddr_phy_ctlr_1 = MT47H128M16RT25E_EMIF_READ_LATENCY,
};

static const struct ddr_data ddr3_256m_data = {
	.datardsratio0 = MT41K128M8JP15E_RD_DQS,
	.datawdsratio0 = MT41K128M8JP15E_WR_DQS,
	.datafwsratio0 = MT41K128M8JP15E_PHY_FIFO_WE,
	.datawrsratio0 = MT41K128M8JP15E_PHY_WR_DATA,
	.datadldiff0 = PHY_DLL_LOCK_DIFF,
};

static const struct cmd_control ddr3_256m_cmd_ctrl_data = {
	.cmd0csratio = MT41K128M8JP15E_RATIO,
	.cmd0dldiff = MT41K128M8JP15E_DLL_LOCK_DIFF,
	.cmd0iclkout = MT41K128M8JP15E_INVERT_CLKOUT,

	.cmd1csratio = MT41K128M8JP15E_RATIO,
	.cmd1dldiff = MT41K128M8JP15E_DLL_LOCK_DIFF,
	.cmd1iclkout = MT41K128M8JP15E_INVERT_CLKOUT,

	.cmd2csratio = MT41K128M8JP15E_RATIO,
	.cmd2dldiff = MT41K128M8JP15E_DLL_LOCK_DIFF,
	.cmd2iclkout = MT41K128M8JP15E_INVERT_CLKOUT,
};

static struct emif_regs ddr3_256m_emif_reg_data = {
	.sdram_config = MT41K128M8JP15E_EMIF_SDCFG,
	.ref_ctrl = MT41K128M8JP15E_EMIF_SDREF,
	.sdram_tim1 = MT41K128M8JP15E_EMIF_TIM1,
	.sdram_tim2 = MT41K128M8JP15E_EMIF_TIM2,
	.sdram_tim3 = MT41K128M8JP15E_EMIF_TIM3,
	.zq_config = MT41K128M8JP15E_ZQ_CFG,
	.emif_ddr_phy_ctlr_1 = MT41K128M8JP15E_EMIF_READ_LATENCY,
};

static const struct ddr_data ddr3_512m_data = {
	.datardsratio0 = MT41K256M8DA125_RD_DQS,
	.datawdsratio0 = MT41K256M8DA125_WR_DQS,
	.datafwsratio0 = MT41K256M8DA125_PHY_FIFO_WE,
	.datawrsratio0 = MT41K256M8DA125_PHY_WR_DATA,
	.datauserank0delay = MT41K256M8DA125_PHY_RANK0_DELAY,
	.datadldiff0 = PHY_DLL_LOCK_DIFF,
};

static const struct cmd_control ddr3_512m_cmd_ctrl_data = {
	.cmd0csratio = MT41K256M8DA125_RATIO,
	.cmd0dldiff = MT41K256M8DA125_DLL_LOCK_DIFF,
	.cmd0iclkout = MT41K256M8DA125_INVERT_CLKOUT,

	.cmd1csratio = MT41K256M8DA125_RATIO,
	.cmd1dldiff = MT41K256M8DA125_DLL_LOCK_DIFF,
	.cmd1iclkout = MT41K256M8DA125_INVERT_CLKOUT,

	.cmd2csratio = MT41K256M8DA125_RATIO,
	.cmd2dldiff = MT41K256M8DA125_DLL_LOCK_DIFF,
	.cmd2iclkout = MT41K256M8DA125_INVERT_CLKOUT,
};

static struct emif_regs ddr3_512m_emif_reg_data = {
	.sdram_config = MT41K256M8DA125_EMIF_SDCFG,
	.ref_ctrl = MT41K256M8DA125_EMIF_SDREF,
	.sdram_tim1 = MT41K256M8DA125_EMIF_TIM1,
	.sdram_tim2 = MT41K256M8DA125_EMIF_TIM2,
	.sdram_tim3 = MT41K256M8DA125_EMIF_TIM3,
	.zq_config = MT41K256M8DA125_ZQ_CFG,
	.emif_ddr_phy_ctlr_1 = MT41K256M8DA125_EMIF_READ_LATENCY,
};

static const struct ddr_data ddr3_1024m_data = {
	.datardsratio0 = MT41K512M8RH125_RD_DQS,
	.datawdsratio0 = MT41K512M8RH125_WR_DQS,
	.datafwsratio0 = MT41K512M8RH125_PHY_FIFO_WE,
	.datawrsratio0 = MT41K512M8RH125_PHY_WR_DATA,
	.datauserank0delay = MT41K512M8RH125_PHY_RANK0_DELAY,
	.datadldiff0 = PHY_DLL_LOCK_DIFF,
};

static const struct cmd_control ddr3_1024m_cmd_ctrl_data = {
	.cmd0csratio = MT41K512M8RH125_RATIO,
	.cmd0dldiff = MT41K512M8RH125_DLL_LOCK_DIFF,
	.cmd0iclkout = MT41K512M8RH125_INVERT_CLKOUT,

	.cmd1csratio = MT41K512M8RH125_RATIO,
	.cmd1dldiff = MT41K512M8RH125_DLL_LOCK_DIFF,
	.cmd1iclkout = MT41K512M8RH125_INVERT_CLKOUT,

	.cmd2csratio = MT41K512M8RH125_RATIO,
	.cmd2dldiff = MT41K512M8RH125_DLL_LOCK_DIFF,
	.cmd2iclkout = MT41K512M8RH125_INVERT_CLKOUT,
};

static struct emif_regs ddr3_1024m_emif_reg_data = {
	.sdram_config = MT41K512M8RH125_EMIF_SDCFG,
	.ref_ctrl = MT41K512M8RH125_EMIF_SDREF,
	.sdram_tim1 = MT41K512M8RH125_EMIF_TIM1,
	.sdram_tim2 = MT41K512M8RH125_EMIF_TIM2,
	.sdram_tim3 = MT41K512M8RH125_EMIF_TIM3,
	.zq_config = MT41K512M8RH125_ZQ_CFG,
	.emif_ddr_phy_ctlr_1 = MT41K512M8RH125_EMIF_READ_LATENCY,
};

/**
 * MityARM335X has a couple of DDR3 configuraiton options
 * Config
 * 0	- 256 MB ->  2 x MT41K128M8JP-125:G
 *                   2 x [128M x 8] = 256MB DDR3 RAM
 * 1	- 512 MB ->  2 x MT41K256M8DA-125:M
 *                   2 x [256M x 8] = 512MB DDR3 RAM
 * 2	- 1024 MB -> 2 x MT41K512M8RH-125:M
 *                   2 x [512M x 8] = 1024MB DDR3 RAM
 *
 * MT41K128M8JP
 * - 14  Addr lines            A[13:0]
 * - 3   bank address lines    BA[2:0]
 * - 10  Column address lines  A[9:0]
 * - CAS Latency = 5
 * - CWL Latency = 5
 *
 * MT41K256M8DA
 * - 15  Addr lines            A[14:0]
 * - 3   bank address lines    BA[2:0]
 * - 10  Column address lines  A[9:0]
 * - CAS Latency = 5
 * - CWL Latency = 5
 *
 */
static void config_am335x_ddr3(int bus_speed, int config)
{
	tfenter;

	switch (config) {
	case RAM_SIZE_512MB_DDR3:
		{
			config_ddr(bus_speed, MT41K256M8DA125_IOCTRL_VALUE,
				   &ddr3_512m_data, &ddr3_512m_cmd_ctrl_data,
				   &ddr3_512m_emif_reg_data, 0);
			ram_size = 512 * 1024 * 1024;
		}
		break;
	case RAM_SIZE_256MB_DDR3:
		{
			config_ddr(bus_speed, MT41K128M8JP15E_IOCTRL_VALUE,
				   &ddr3_256m_data, &ddr3_256m_cmd_ctrl_data,
				   &ddr3_256m_emif_reg_data, 0);
			ram_size = 256 * 1024 * 1024;
		}
		break;
	case RAM_SIZE_1GB_DDR3:
		{
			config_ddr(bus_speed, MT41K512M8RH125_IOCTRL_VALUE,
				   &ddr3_1024m_data, &ddr3_1024m_cmd_ctrl_data,
				   &ddr3_1024m_emif_reg_data, 0);
			ram_size = 1024 * 1024 * 1024;
		}
		break;
	default:
		printf("%s:%d  %s Unknown RAM configuration %d\n", __FILE__,
		       __LINE__, __func__, config);
		break;
	}
}

/**
 * MityARM335X uses the Micron Mt47H128M16RT-25E for DDR2
 * 16M x 16 x 8 bank [128M x 16 = 256MB] DDR2 RAM
 * This part has:
 * 14 Addr lines            A[13:0]
 * 3 bank address lines    BA[2:0]
 * 10 Column address lines  A[9:0]
 * CaS Latency = 5
 */
static void config_am335x_ddr2(int bus_speed)
{
	tfenter;

	config_ddr(bus_speed, MT47H128M16RT25E_IOCTRL_VALUE, &ddr2_data,
		   &ddr2_cmd_ctrl_data, &ddr2_emif_reg_data, 0);
	ram_size = 256 * 1024 * 1024;
}

#endif /* CONFIG_SPL_BUILD */

/**
 * Check model number to make sure u-boot
 * is configured correctly. 
 */
void check_nand_support(void)
{
#ifndef CONFIG_MITYARM_1GB_NAND
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_1GB)
		printf("Critical Link: Error u-boot not compiled with for 1GB nand support. Nand may not function correctly.\n");
#endif
	/* 512MB Nand requires specific changes enabled by CONFIG_4K_PAGE_NAND.  */
#if !defined(CONFIG_MITYARM_4K_PAGE_NAND) || defined(CONFIG_MITYARM_1GB_NAND)
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_512MB)
		printf("Critical Link: Error u-boot not compiled with for 512MB nand support. Nand may not function correctly.\n");
#endif
	/* 256MB Nand requires 2K Page setup */
#ifdef CONFIG_MITYARM_4K_PAGE_NAND
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_256MB)
		printf("Critical Link: Error u-boot compiled for large page nand support. Nand may not function correctly.\n");
#endif
}

#ifndef NUM_EEPROM_RETRIES
#define NUM_EEPROM_RETRIES	2
#endif

/*
 * Read header information from EEPROM into global structure.
 */
int read_eeprom(void)
{
	tfenter;

	int retries, rv;

	for (retries = 0; retries <= NUM_EEPROM_RETRIES; ++retries) {
		if (retries)
			printf("Retrying [%d] ...\n", retries);

		/* Check if baseboard eeprom is available */
		i2c_set_bus_num(I2C_EEPROM_BUS);
		rv = i2c_probe(I2C_EEPROM_ADDR);
		if (rv) {
			printf
			    ("Could not probe the EEPROM; something "
			     "fundamentally wrong on the I2C bus.\n");
			continue;	/* retry */
		}

		/* try and read our configuration block */
		rv = get_factory_config_block();
		if (rv < 0) {
			printf("I2C Error reading factory config block\n");
			continue;	/* retry */
		}
		/* No I2C issues, data was either OK or not entered... */
		if (rv > 0)
			printf("Bad I2C configuration found\n");
		break;
	}
	factory_config_block.ModelNumber[31] = '\0';

#if defined(CONFIG_SPL_BUILD)
	if (!rv)
		printf
		    ("MitySOM335x profile %d - Model No: %s Serial No: %d\n",
		     profile, factory_config_block.ModelNumber,
		     factory_config_block.SerialNumber);
#endif

	return rv;
}

#if defined(CONFIG_SPL_BUILD) && defined(CONFIG_SPL_BOARD_INIT)

void am33xx_spl_board_init(void)
{
	int ii = 0;
	uchar buf[4];
	int sil_rev;
	int mpu_vdd;
	tfenter;


	/* Get the frequency */
	dpll_mpu_opp100.m = am335x_get_efuse_mpu_max_freq(cdev);

	/* Configure the i2c1 and 2 pin mux */
	enable_i2c1_pin_mux();
	enable_i2c2_pin_mux();
#ifdef CONFIG_SPL_ENET_SUPPORT
	mem_malloc_init(0x82000000, 0x00100000);
	set_default_env(NULL);
#endif

#ifdef CONFIG_SPL_MMC_SUPPORT
	enable_mmc0_pin_mux();
#endif
	configure_evm_pin_mux(profile);

	printf
	    ("Critical Link AM335X %s -- NAND Page size = %dk booting from dev %x\n",
	     (PROFILE_1 == profile) ? "Test Fixture" : "Dev Kit",
	     nand_page_size + 1 / 1024, spl_boot_device());

	/*
	 * EVM PMIC code.  All boards currently want an MPU voltage
	 * of 1.2625V and CORE voltage of 1.1375V to operate at
	 * 720MHz.
	 */
	i2c_set_bus_num(I2C_TPS65910_CTL_BUS);	/* calls i2c_init... */
	if (i2c_probe(TPS65910_CTRL_I2C_ADDR)) {
		printf("No PMIC At I2C addr %d\n", TPS65910_CTRL_I2C_ADDR);
		for (; ii < 128; ++ii)
			if (0 == i2c_probe(ii))
				printf("I2C device found at addr %d\n", ii);
		return;
	}

	set_led_d3(1);

	/* Turn off pull down resistors for BOOT0P, BOOT1P.  Som has
	 * pullup resistor which wastes power. */
	buf[0] = 0;
	if(0 != i2c_read(TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG, 1, buf, 1)) {
		printf("Unable to read I2C reg %d:%d.%d %x\n",
		       i2c_get_bus_num(), TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG,
		       buf[0]);
	}
	buf[0] &= TPS65910_PUADEN_REG_BOOTP_PD_DISABLE;

	if (0 != i2c_write(TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG, 1, buf, 1)) {
		printf("Unable to write I2C reg %d:%d.%d %x\n",
		       i2c_get_bus_num(), TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG,
		       buf[0]);
	}


	/* Get the frequency */
	dpll_mpu_opp100.m = am335x_get_efuse_mpu_max_freq(cdev);

	/*
	 * Depending on MPU clock and PG we will need a different
	 * VDD to drive at that speed.
	 */
	sil_rev = readl(&cdev->deviceid) >> 28;
	mpu_vdd = am335x_get_tps65910_mpu_vdd(sil_rev,
			dpll_mpu_opp100.m);

	/* Tell the TPS65910 to use i2c */
	tps65910_set_i2c_control();

	/* First update MPU voltage. */
	if (tps65910_voltage_update(MPU, mpu_vdd))
		return;

	/* Second, update the CORE voltage. */
	if (tps65910_voltage_update(CORE, TPS65910_OP_REG_SEL_1_1_3))
		return;

	/* Set CORE Frequencies to OPP100 */
	do_setup_dpll(&dpll_core_regs, &dpll_core_opp100);

	/* Set MPU Frequency to what we detected now that voltages are set */
	do_setup_dpll(&dpll_mpu_regs, &dpll_mpu_opp100);

	/* Switch NAND default configuration so we can load u-boot from NAND */
	//switch (factory_config_block.ModelNumber[NAND_SIZE_POSITION]) {
	//case NAND_SIZE_256MB:
	//	omap_nand_switch_ecc(1, 8);	/* HW-BCH8 ECC */
	//	break;
	//case NAND_SIZE_512MB:
	//	omap_nand_switch_ecc(1, 16);	/* HW-BCH16 ECC */
	//	break;
	//default:
	//	omap_nand_switch_ecc(0, 0);	/* SW ECC */
	//	break;
	//}
	//omap_nand_print_ecc_info();

#if defined(CONFIG_SPL_ENET_SUPPORT) && defined(CONFIG_SPL_BUILD)
	miiphy_init();
	board_eth_init(NULL);
#endif

}
#endif

#ifdef CONFIG_SPL_BUILD
static void prompt_for_mem(void)
{
	int OK;
	tfenter;

	printf("board_init: unable to read eeprom\n");
	do {
		OK = 1;
		printf("Enter Memory Config:\n"
		       "	0: DDR2\n"
		       "	1: DDR3 (256 MB)\n"
		       "	2: DDR3 (512 MB)\n" "	3: DDR3 (1 GB)\n");
		switch (serial_getc()) {
		case '0':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_256MB_DDR2;
			break;
		case '1':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_256MB_DDR3;
			break;
		case '2':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_512MB_DDR3;
			break;
		case '3':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_1GB_DDR3;
			break;
		default:
			OK = 0;
			printf("Invalid code!\n");
			break;
		}
	} while (!OK);
}

/* Used when setting up memory.  If config is uncertain, trigger a mtest */
static bool __attribute__((section (".data"))) need_mtest = false;

#define OSC	(V_OSCK/1000000)
const struct dpll_params dpll_ddr2 = {
		266, OSC-1, 1, -1, -1, -1, -1};
const struct dpll_params dpll_ddr3 = {
		400, OSC-1, 1, -1, -1, -1, -1};

/* Return ddr bus speed parameters */
const struct dpll_params *get_dpll_ddr_params(void)
{
	tfenter;

	/* set these up to skip reading from EEPROM and just force the config */
#ifdef  CONFIG_ASK_RAM
#if	defined(CONFIG_DDR3_256MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_256MB_DDR3;
#elif	defined(CONFIG_DDR3_512MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_512MB_DDR3;
#elif	defined(CONFIG_DDR3_1GB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] = RAM_SIZE_1GB_DDR3;
#elif	defined(CONFIG_DDR2_256MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_256MB_DDR2;
#else
	/* if none selected, then prompt every time */
	prompt_for_mem();
#endif /* CONFIG_DDR* */
	need_mtest = true;
#else /* !CONFIG_ASK_RAM */
	/* read factory configuration number to determine memory type */
	enable_i2c1_pin_mux();
	if (0 != read_eeprom()) {
		/* bad read, make sure data is "bogus" for logic below */
		factory_config_block.ConfigVersion = 0;
		factory_config_block.ModelNumber[RAM_SIZE_POSITION] = ' ';
	}
	else {
		check_nand_support();
	}

	/* if old configuration, this has to be a DDR2 256 MB part,
	 * we need this check because a handful of units were not
	 * configured with the proper DDR code during the initial
	 * build and some units made it into the wild for early
	 * access customers */
	if (factory_config_block.ConfigVersion == CONFIG_I2C_VERSION_1_1) {
		printf("Old factory config found, "
		       "configuring for 256 MB DDR2\n");
		factory_config_block.ModelNumber[RAM_SIZE_POSITION] = RAM_SIZE_256MB_DDR2;
	}

	switch (factory_config_block.ModelNumber[RAM_SIZE_POSITION]) {
	case RAM_SIZE_256MB_DDR3:
	case RAM_SIZE_512MB_DDR3:
	case RAM_SIZE_1GB_DDR3:
	case RAM_SIZE_256MB_DDR2:
		break;
	default:
		prompt_for_mem();
		need_mtest = true;
		break;
	}
#endif /* CONFIG_ASK_RAM */

	switch(factory_config_block.ModelNumber[RAM_SIZE_POSITION]) {
	case RAM_SIZE_256MB_DDR3:
	case RAM_SIZE_512MB_DDR3:
	case RAM_SIZE_1GB_DDR3:
		return &dpll_ddr3;
	case RAM_SIZE_256MB_DDR2:
	default:
		return &dpll_ddr2;
	}
}

void set_uart_mux_conf(void)
{
	tfenter;
	enable_uart0_pin_mux();
}

void set_mux_conf_regs(void)
{
	tfenter;
	/* Configure the i2c1 and 2 pin mux */
	enable_i2c1_pin_mux();
	enable_i2c2_pin_mux();

	configure_evm_pin_mux(profile);
}

void sdram_init(void) {
	char ram_type;
	int bus_speed_ddr2 = 266;
	int bus_speed_ddr3 = 400;

	tfenter;
	ram_type = factory_config_block.ModelNumber[RAM_SIZE_POSITION];

	/* get the type from the position in RAM */
	switch (ram_type) {
	case RAM_SIZE_512MB_DDR3:
		printf("Configuring for 512 MB DDR3 @ %dMHz\n", bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	case RAM_SIZE_256MB_DDR2:
		printf("Configuring for 256 MB DDR2 @ %dMHz\n", bus_speed_ddr2);
		config_am335x_ddr2(bus_speed_ddr2);
		break;
	case RAM_SIZE_256MB_DDR3:
		printf("Configuring for 256 MB DDR3 @ %dMHz\n", bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	case RAM_SIZE_1GB_DDR3:
		printf("Configuring for 1024 MB DDR3 @ %dMHz\n",
		       bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	default:		/* for now as we get boards to test this should
				   become NOT_REACHED */
		printf("Don't know how to handle memory type!\n");
		need_mtest = true;
		break;
	}
	if (256 * 1024 * 1024 > ram_size)
		ram_size = 256 * 1024 * 1024;
	if (need_mtest) {
		int mtest_ret = 0;
#if 0
		/* Test whole ram */
		printf("ram size = %dMB, start = %08x end = %08x\n",
		       ram_size / 1024, CONFIG_SYS_SDRAM_BASE,
		       CONFIG_SYS_SDRAM_BASE + ram_size);
		mtest_ret =
		    mtest((ulong *) CONFIG_SYS_SDRAM_BASE,
			  (ulong *) (CONFIG_SYS_SDRAM_BASE + ram_size));
#else
		/* Test first 1024 Bytes */
		printf("ram size = %dMB, start = %08x end = %08x\n",
		       (int)(ram_size / 1024), CONFIG_SYS_MEMTEST_START,
		       CONFIG_SYS_MEMTEST_END);
		mtest_ret =
		    mtest((ulong *)CONFIG_SYS_MEMTEST_START,
			  (ulong *)(CONFIG_SYS_MEMTEST_START + 1024));
#endif
		if (mtest_ret) {	/* mtest failed */
			printf("Memory Test failure.. board defect!\n");
			hang();
		}
	}

}
#endif /* CONFIG_SPL_BUILD */

int board_init(void)
{
	tfenter;

#ifndef CONFIG_SPL_BUILD
	if (0 != read_eeprom()) {
		printf("board_init:unable to read eeprom\n");
	}
	else {
		check_nand_support();
	}
#endif

	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;

	gpmc_init();

	return 0;
}

int misc_init_r(void)
{
	tfenter;

	return 0;
}

#ifdef CONFIG_DRIVER_TI_CPSW
#ifndef CONFIG_SPL_BUILD
static void cpsw_control(int enabled)
{
	/* Enable/disable the RGMII2 port */

	tfenter;

	return;
}

static struct cpsw_slave_data cpsw_slaves[] = {
	{
	 .slave_reg_ofs = 0x208,
	 .sliver_reg_ofs = 0xd80,
#ifdef CONFIG_SPL_ENET_SUPPORT
	 .phy_id = 1,
#else
	 .phy_id = 2,
#endif
	 },
	{
	 .slave_reg_ofs = 0x308,
	 .sliver_reg_ofs = 0xdc0,
#ifdef CONFIG_SPL_ENET_SUPPORT
	 .phy_id = 2,
#else
	 .phy_id = 1,
#endif
	 },
};

static struct cpsw_platform_data cpsw_data = {
	.mdio_base = CPSW_MDIO_BASE,
	.cpsw_base = CPSW_BASE,
	.mdio_div = 0xff,
	.channels = 8,
	.cpdma_reg_ofs = 0x800,
	.slaves = 2,
	.slave_data = cpsw_slaves,
	.ale_reg_ofs = 0xd00,
	.ale_entries = 1024,
	.host_port_reg_ofs = 0x108,
	.hw_stats_reg_ofs = 0x900,
	.bd_ram_ofs = 0x2000,
	.mac_control = (1 << 5) /* MIIEN */ ,
	.control = cpsw_control,
	.host_port_num = 0,
	.version = CPSW_CTRL_VERSION_2,
};
#endif /* !CONFIG_SPL_BUILD */

int board_eth_init(bd_t *bis)
{
	int rv = 0;
#ifndef CONFIG_SPL_BUILD
	uint8_t mac_addr[6];
	uint32_t mac_hi, mac_lo;
	int valid_mac = 0;

	tfenter;


	gpio_request(GPIO_PHY_RESET_N, "gpio3_10");
	// Pull into reset
	gpio_direction_output(GPIO_PHY_RESET_N, 0);
	udelay(100);
	// Pull out of reset
	gpio_set_value(GPIO_PHY_RESET_N, 1);

	/*
	 * Environment set MAC address trumps all...
	 */
	if (eth_getenv_enetaddr("ethaddr", mac_addr))
		valid_mac = 1;
	/*
	 * Factory configuration MAC address overides built-in
	 */
	else if (is_valid_ether_addr(factory_config_block.MACADDR)) {
		memcpy(mac_addr, factory_config_block.MACADDR, 6);
		eth_setenv_enetaddr("ethaddr", mac_addr);
		valid_mac = 1;
	}
	/* Fall back to MAC address in the eFuse eeprom in part */
	else {

		debug("<ethaddr> not set. Reading from E-fuse\n");
		/* try reading mac address from efuse */
#if (CONFIG_ETH_PORT == 1)
		mac_lo = readl(&cdev->macid0l);
		mac_hi = readl(&cdev->macid0h);
#else
		mac_lo = readl(&cdev->macid1l);
		mac_hi = readl(&cdev->macid1h);
#endif
		mac_addr[0] = mac_hi & 0xFF;
		mac_addr[1] = (mac_hi & 0xFF00) >> 8;
		mac_addr[2] = (mac_hi & 0xFF0000) >> 16;
		mac_addr[3] = (mac_hi & 0xFF000000) >> 24;
		mac_addr[4] = mac_lo & 0xFF;
		mac_addr[5] = (mac_lo & 0xFF00) >> 8;

		if (is_valid_ether_addr(mac_addr)) {
			eth_setenv_enetaddr("ethaddr", mac_addr);
			valid_mac = 1;
		}
	}
	if (valid_mac)
	{
		writel(RGMII_MODE_ENABLE, &cdev->miisel);
		cpsw_slaves[0].phy_if = cpsw_slaves[1].phy_if =
				PHY_INTERFACE_MODE_RGMII;
	}
	else
	{
		printf("Warning: MAC Address not found. Ethernet disabled\n");
	}

	rv = cpsw_register(&cpsw_data);
	if (rv < 0)
		printf("Error %d registering CPSW switch\n", rv);
#endif
	return rv;
}
#endif

#ifndef CONFIG_SPL_BUILD

/******************************************************************************
 * Command to switch between NAND HW and SW ecc
 *****************************************************************************/
static int do_switch_ecc(cmd_tbl_t *cmdtp, int flag, int argc,
			 char *const argv[])
{
	tfenter;
	if (argc < 2) {
		omap_nand_print_ecc_info();
		return 0;
	}
	printf("nandecc disabled\n");
#if 0
	if (strncmp(argv[1], "hw", 2) == 0) {
		int type = 0;
		if (argc == 3)
			type = simple_strtoul(argv[2], NULL, 10);
		switch (type) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 8:
		case 16:
			omap_nand_switch_ecc(1, type);
			break;
		default:
			goto usage;
			break;
		}
	} else if (strncmp(argv[1], "sw", 2) == 0)
		omap_nand_switch_ecc(0, 0);
	else
		goto usage;

#endif
	return 0;

#if 0
usage:
	printf("Usage: nandecc %s\n", cmdtp->usage);
	return 1;
#endif
}

U_BOOT_CMD(nandecc, 3, 1, do_switch_ecc,
	"Switch NAND ECC calculation algorithm b/w hardware and software",
	"[sw|hw <hw_type>] \n"
	"   [sw|hw]- Switch b/w hardware(hw) & software(sw) ecc algorithm\n"
	"   hw_type- 0 for Hamming code\n"
	"            1 for bch4\n"
	"            2 for bch8\n" "            3 for bch16\n");

#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{
	tfenter;

	/* Turn diagnostic led off to indicate configuration complete */
	set_led_d3(0);

	/* TODO Could set environment variables such as board model number,
	 * serial number.  See am335x_evm/board.c for example */
	return 0;
}
#endif /* CONFIG_BOARD_LATE_INIT */
#endif /* CONFIG_SPL_BUILD */
